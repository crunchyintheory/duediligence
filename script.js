// Would have used Angular but the setup would have taken too long.

const baseheight = 1024;
const basewidth = 504;

const GameState = {
    NotStarted: 0,
    InPlay: 1,
    Finished: 2,
    ShowingAlarmClock: 3,
    EndOfDay: 4
}

var gameState = GameState.NotStarted;

var phoneCallPromised = false;

const dialogueTrees = [
    {
        mainline: [
            {
                message: "Hey! Are you busy today?",
                choices: [
                    {
                        message: "Yes",
                        consequence: {
                            message: "Okay, well if you get a chance can you give me a call?"
                        }
                    },
                    {
                        message: "(Lie) No",
                        events: () => {
                            dialogueTrees[0].mainline[1].choices[0].disabled = true
                            dialogueTrees[0].mainline[1].choices[1].events = () => {
                                phoneCallPromised = true;
                                getStatusMessage("You promised your mom a phone call", { family: -4 });
                            }
                        },
                        consequence: {
                            message: "Great! Could you give me a call please?"
                        }
                    }
                ]
            },
            {
                choices: [
                    {
                        message: "Sorry, I can't, I really have to get some school work done",
                        stats: {
                            family: -1
                        },
                        consequence: {
                            message: "That's okay, good luck."
                        }
                    },
                    {
                        message: "Sure thing"
                    }
                ]
            }
        ]
    },
    {
        mainline: [
            {
                message: "By the way, I forgot to ask you"
            },
            {
                message: "How did your doctor's appointment go yesterday?",
                choices: [
                    {
                        message: "I'm sorry, I couldn't make it.",
                        stats: {
                            family: -1
                        },
                        consequence: {
                            message: "I just called and they managed to squeeze you in for today, so that should work out!",
                        },
                        events: () => {
                            end = dialogueTrees[1].mainline.splice(2)
                            dialogueTrees[1].mainline = dialogueTrees[1].mainline.concat([
                                {
                                    choices: [
                                        {
                                            message: "(Lie) Okay, I'll make it this time, I promise",
                                            consequence: {
                                                message: "Alright, they told me they'd call me if they need to reschedule again."
                                            },
                                            stats: {
                                                energy: -2
                                            },
                                            events: () => {
                                                dialogueTrees[2].mainline[2].message = "I know you haven't been going to therapy, and I'm getting really worried."
                                            }
                                        },
                                        {
                                            message: "Okay, I'll make it this time, I promise",
                                            time: 3
                                        }
                                    ]
                                }
                            ]).concat(end);
                        }
                    },
                    {
                        message: "(Lie) It was fine",
                        stats: {
                            energy: -1
                        }
                    }
                ]
            },
            {
                message: "I hope this therapist ends up working out for you ❤️"
            }
        ]
    },
    {
        mainline: [
            {
                message: "Hey",
                choices: [
                    {
                        message: "What's up?"
                    }
                ]
            },
            {
                message: "Well, you seem a bit off lately"
            },
            {
                message: "I know you've been seeing this new therapist and I don't want to pressure you or anything, but"
            },
            {
                message: "Are you okay?",
                choices: [
                    {
                        message: "Honestly, no, I'm not. I feel like I can't keep up with everything I'm supposed to be doing, while everyone around me is doing it perfectly. I don't know what I'm doing wrong...",
                        stats: {
                            family: +2,
                            energy: -8
                        },
                        consequence: {
                            message: "Okay, well let me know if there's something I can do to help. I'm always here if you need me."
                        },
                        events: () => {
                            dialogueTrees.forEach(x => { if(x.goodPath != undefined) x.mainline = x.goodPath; });
                            dialogueTrees[2].mainline.push({ message: "I know time can be really hard to juggle, I struggled with it too. It'll be okay, I believe in you." });
                        }                   
                    },
                    {
                        message: "(Lie) I'm fine, really. Don't worry mom.",
                        stats: {
                            family: -2
                        },
                        consequence: {
                            message: "Okay, I'm sorry, I guess I was worried about nothing."
                        },
                        events: () => {
                            dialogueTrees.forEach(x => { if(x.badPath != undefined) x.mainline = x.badPath; });
                        }    
                    }
                ]
            }
        ]
    },
    {
        mainline: [
            {
                message: "Hey, I just spoke with your sister. Her partner broke up with her, she could really use a call."
            },
            {
                message: "Not that you care, clearly, since you won't even answer MY texts. Whatever. Have fun with whatever you're doing."
            }
        ],
        goodPath: [
            {
                message: "Hey, I just spoke with your sister. Her partner broke up with her, she could really use a call."
            },
            {
                message: "I understand if you can't though, I told her that you're struggling right now and she understands.",
                choices: [
                    {
                        message: "Thanks Mom, I'll try to call her if I can."
                    },
                    {
                        message: "Thanks Mom, tell her I'm sorry for me."
                    }
                ]
            }
        ],
        badPath: [
            {
                message: "Hey, I just spoke with your sister. Her partner broke up with her, she could really use a call."
            },
            {
                message: "Please call her",
                choices: [
                    {
                        message: "Okay",
                        events: () => {
                            dialogueTrees[0].mainline[1].choices[0].disabled = true
                            dialogueTrees[0].mainline[1].choices[1].events = () => {
                                phoneCallPromised = true;
                                getStatusMessage("You promised your sister a phone call", { family: -4 });
                            }
                        }
                    },
                    {
                        message: "I'm sorry, I can't. Tell her I'm sorry for me.",
                        consequence: {
                            message: "Seriously? What could you possibly have that is more important than consoling your sister at a time like this? You were fine yesterday."
                        }
                    }
                ]
            }
        ]
    }
];

const daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

const days = [
    {
        events: () => {
            getMail("Assignment Reminder", "Mon 05:40 AM", "Hello class, this message is just to remind you that your first assignment is due tonight, so make sure to turn it in on time!", () => {
                addAssignment("First Assignment", 0, 3);
            });
            addAssignment("Final Project 1", 3, 8);
            addAssignment("Final Project 2", 3, 8);
        }
    },
    {
        events: () => {
            getMail("Art Project", "Tue 06:57 AM", "Hello class, I've uploaded new details for the new project as discussed in class. This is due on Wednesday, so make sure you don't wait on starting it.", () => {
                addAssignment("Art Project", 2, 12);
            });
            getMail("History Paper: Due Thurs", "Tue 07:59 AM", "Class. I have assigned a new history paper and I expect its prompt completion by this Thursday. All late papers will receive a 0. That is all. ", () => {
                addAssignment("History Paper", 3, 4);
            });
        }
    },
    {
        events: () => {
            getMail("Correction: Art Project", "Wed 01:30 AM", "I'm so sorry, I just realized that in the last message I wasn't very clear. This project is due *next* Wendesday, not this Wednesday, which I guess is now today!", () => {
                $(`.mail-inbox-message[data-id="Tue 06:57 AM"]`).remove();
                const id = assignments.findIndex(x => x.name == "Art Project");
                if(id != -1) {
                    assignments[id].progress = assignments[id].numHrs;
                    $(`.planner-task[data-id=${id}] .planner-task-due`).text("Due NEXT Wendesday 11:59 PM")
                    $(`.planner-task[data-id=${id}]`).addClass("completed");
                }
                const length = assignments.filter(x => x.progress < x.numHrs && x.dueDay >= x.currentDay).length;
                $("#app-icons .app-button[data-app='planner'] .app-notification-badge").attr("data-count", length).find(".badge-text").text(length);
            });
            getMail("Calculus Homework", "Wed 06:39 AM", "Hi everyone, here is the next problem set you should be working on in advance of tomorrow's class!", () => {
                addAssignment("Calculus Homework", 3, 4);
            });
        }
    },
    {
        events: () => {
            getMail("Pop Quiz", "Thu 05:40 AM", "Please complete this ASAP as it won't be open for long!", () => {
                addAssignment("Pop Quiz", 3, 1);
            });
        }
    }
];

const assignments = [];

var notifications = {
    messages: {
        count: 0,
        template: (count) => `You have ${count} unread message${count > 1 ? "s" : ""}.`
    },
    mail: {
        count: 0,
        template: (count) => `You have ${count} unread email${count > 1 ? "s" : ""}.`
    },
    planner: {
        count: 0,
        template: (count) => `You have ${count} pending task${count > 1 ? "s" : ""}.`
    }
};

var stats = {
    school: 8,
    family: 8,
    energy: 8
};

var currentDay = 0;

var currentTime = 8;

var onresize = () => {
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    const h = vh * 0.9;
    const w = vw * 0.9;
    const finalh = Math.min(h, (baseheight / basewidth) * w);
    const finalw = Math.min(w, (basewidth / baseheight) * h);
    const finalscale = finalw / basewidth;
    $("#phone").css("transform", `scale(${finalscale}) translate(${basewidth / -2}px, ${baseheight / -2}px) `);
}

window.onresize = onresize;

function addMessage(text, isTo) {
    $("#messages-history").append($("<div/>", {
        class: `message ${isTo ? "to" : "from"}`,
        text
    }));
    if(!isTo) {
        let count = $("#app-icons .app-button[data-app='messages'] .app-notification-badge").attr("data-count");
        $("#app-icons .app-button[data-app='messages'] .app-notification-badge").attr("data-count", +count + 1).find(".badge-text").text(+count + 1);
        notifications.messages.count++; 
    }
    else {
        afterSpendTime();
        $("#app-icons .app-button[data-app='messages'] .app-notification-badge").attr("data-count", 0).find(".badge-text").text(0);
    }
    $("#messages-history").scrollTop($("#messages-history").height());
}

function sendMessage(text) {
    return addMessage(text, true);
}

function getMessage(text) {
    return addMessage(text, false);
}

function getStatusMessage(text, stats) {
    statsHtml = Object.entries(stats).map(([key, value]) => {
        return `<span class="stats-${value > 0 ? "p" : "m"}">${key.toUpperCase()} ${value > 0 ? "+" : ""}${value}</span>`
    }).join("");
    $("#messages-history").append($("<div/>", {
        class: `info-message`,
        html: `${text}<div class="stats-delta">${statsHtml} if unfulfilled</div>`
    }));
}

function doTopLevelMessagePropagate(day, index) {
    message = dialogueTrees[day].mainline[index];
    if(message == null) {
        if(dialogueTrees[day].mainline.slice(-1)[0].choices == null) {
            notifications.messages.count = 0;
        }
        $("#app-icons .app-button[data-app='messages'] .app-notification-badge").attr("data-count", 0).find(".badge-text").text(0);
        return;
    }
    if(message.message != null) {
        getMessage(message.message);
    }
    if(message.choices == null)
        return doTopLevelMessagePropagate(day, index + 1);
    setChoices(message.choices, day, index);
}

function setChoices(choices, day, mainline) {
    $("#messages-compose").empty();
    choices.forEach((x, i) => {
        let content = `<span class="inner">${x.message}`;
        if(x.stats) {
            statsHtml = Object.entries(x.stats).map(([key, value]) => {
                return `<span class="stats-${value > 0 ? "p" : "m"}">${key.toUpperCase()} ${value > 0 ? "+" : ""}${value}</span>`
            }).join("");
            content += `<div class="stats-delta">${statsHtml}</div>`
        }
        if(x.time) {
            content += `<div>(${x.time} hour${x.time > 1 ? "s" : ""})</div>`;
        }
        content += "</span>";
        $("#messages-compose").append($("<div/>", {
            class: `compose-choice ${x.disabled ? "choice-disabled" : ""}`,
            click: function() {
                if(x.stats) {
                    let statsValid = true;
                    Object.entries(x.stats).forEach(([stat, value]) => {
                        if(value > 0) return;
                        if(stats[stat] < Math.abs(value)) {
                            statsValid = false;
                        }
                    });
                    if(!statsValid) return;
                }
                notifications.messages.count = 0;
                sendMessage(x.message);
                if(x.events) x.events();
                let j = 0;
                while(j++ < x.time - 1) {
                    afterSpendTime();
                }
                updateStats(x.stats);
                choiceSelect(day, mainline, i);
            },
            html: content
        }));
    });
    setTimeout(() => { $("#messages-history").scrollTop(10000) }, 10);
}

function choiceSelect(day, mainline, choice) {
    $("#messages-compose").empty();
    mainlineMsg = dialogueTrees[day].mainline[mainline];
    if(mainlineMsg.choices[choice].consequence) getMessage(mainlineMsg.choices[choice].consequence.message);
    doTopLevelMessagePropagate(day, mainline + 1);
}

function doDailyMessage(day = null) {
    if(day == null) day = currentDay;
    doTopLevelMessagePropagate(day, 0);
}

function getMail(subject, datetime, content, onRead) {
    const clone = $('template#mail-message-template').contents().clone();
    clone.find(".mail-inbox-message-title").text(subject);
    clone.find(".mail-inbox-message-datetime").text(datetime);
    clone.find(".mail-inbox-message-preview").text(content);
    clone.attr("data-id", datetime);

    clone.appendTo("#mail-inbox-messages");

    clone.on("click", ev => { 
        if(!$(ev.currentTarget).is(".read")) onRead()
        focusMail(ev.currentTarget, subject, datetime, content);
    });

    let count = $("#app-icons .app-button[data-app='mail'] .app-notification-badge").attr("data-count");
        $("#app-icons .app-button[data-app='mail'] .app-notification-badge").attr("data-count", +count + 1).find(".badge-text").text(+count + 1);
    notifications.mail.count++;
}

function focusMail(el, subject, datetime, content) {
    if(!$(el).is(".read")) {
        let count = $("#app-icons .app-button[data-app='mail'] .app-notification-badge").attr("data-count");
        $("#app-icons .app-button[data-app='mail'] .app-notification-badge").attr("data-count", count - 1).find(".badge-text").text(count - 1);
        notifications.mail.count--;
    }
    $(el).addClass("read");
    $("#mail-message-title").text(subject);
    $("#mail-message-info").text(datetime);
    $("#mail-message-content").text(content);
    $("#app-mail").attr("data-screen", "message");
}

function addAssignment(name, dueDay, numHrs) {
    const clone = $("template#planner-task-template").contents().clone();
    clone.find(".planner-task-name").text(name);
    clone.find(".planner-task-due").text(`Due ${daysOfWeek[dueDay]} 11:59 PM`);
    clone.find(".planner-task-status").text(`0 / ${numHrs} hrs`);

    clone.appendTo("#planner-tasks");

    clone.on("click", function() {
        const id = $(this).data("id");
        if(assignments[id].progress >= numHrs || assignments[id].dueDay < currentDay || stats.energy <= 0) return;
        updateStats({ energy: -1 });
        afterSpendTime();
        assignments[id].progress++;
        $(this).find(".planner-task-status").text(`${assignments[id].progress} / ${assignments[id].numHrs} hrs`);
        if(assignments[id].progress >= numHrs) {
            $(this).addClass("completed");
        }
        const length = assignments.filter(x => x.progress < x.numHrs && dueDay >= currentDay).length;
        $("#app-icons .app-button[data-app='planner'] .app-notification-badge").attr("data-count", length).find(".badge-text").text(length);
    });

    const id = assignments.push({name, dueDay, numHrs, progress: 0}) - 1;
    clone.attr("data-id", id);

    if(dueDay < currentDay) {
        clone.addClass("overdue");
        updateStats({ school: -numHrs });
        assignments[id].penalized = true;
    }

    const length = assignments.filter(x => x.progress < x.numHrs && dueDay >= currentDay).length;
    $("#app-icons .app-button[data-app='planner'] .app-notification-badge").attr("data-count", length).find(".badge-text").text(length);
}

var statusStyle = 0;

function updateStats(name = null, delta = 0) {
    if(name != null) {
        if(typeof(name) == "object") {
            Object.entries(name).forEach(([n, delta]) => {
                stats[n] = Math.min(8, stats[n] + delta);
            });
        }
        else {
            stats[name] = Math.min(8, stats[name] + delta);
        }
    }

    if(stats.school <= 0 || stats.family <= 0) {
        return gameOver();
    }

    $("#statuses").empty();
    Object.entries(stats).forEach(([key, value]) => {
        transformed = key.split("_").map(x => x.toUpperCase().slice(0, 1) + x.slice(1)).join(" ");
        $("#statuses").append($("<div/>", {
            text: `${transformed}:`
        }));
        if(statusStyle == 0) {
            $("#statuses").append($(`<div>${(new Array(value)).fill("&#9608;").join(" ")}</div>`));
        }
        else {
            $("#statuses").append($("<div/>", {
                text: value
            }));
        }
    });
}

function updateNotifications() {
    $("#notification-tray").empty();
    notifications.planner.count = assignments.filter(x => x.progress < x.numHrs && x.dueDay >= currentDay).length;
    Object.entries(notifications).filter(([_, {count}]) => count > 0).forEach(([app, {count, template}]) => {
        const clone = $("template#notification").contents().clone();
        clone.attr("data-app", app);
        clone.prepend($(`.app-button[data-app="${app}"] .app-icon`).clone());
        clone.find(".app-icon").removeClass("app-icon").addClass("notification-icon");
        clone.find(".notification-title").text($(`.app-button[data-app="${app}"] .app-name`).text());
        clone.find(".notification-text").text(template(count));

        clone.appendTo($("#notification-tray"));
    });
}

function afterSpendTime() {
    $("#statusbar-left").text(`${(++currentTime % 12) == 0 ? 12 : (currentTime % 12)}:00 ${(currentTime / 12 >= 1) ? "PM" : "AM"}`);
    $("#battery-icon").removeClass().addClass("fas");
    if(currentTime >= 19) {
        $("#battery-icon").addClass("fa-battery-empty");
    }
    else if(currentTime >= 17) {
        $("#battery-icon").addClass("fa-battery-quarter");
    }
    else if(currentTime >= 14) {
        $("#battery-icon").addClass("fa-battery-half");
    }
    else if(currentTime >= 11) {
        $("#battery-icon").addClass("fa-battery-three-quarters");
    }
    else {
        $("#battery-icon").addClass("fa-battery-full");
    }

    if(currentTime >= 20) {
        if(days.length == currentDay + 1) {
            gameOver();
        }
        else {
            showEndOfDayScreen();
        }
    }
}

function gameOver() {
    let screenToShow;
    if(stats.school <= 0 || stats.family <= 0) {
        // Bad Ending
        screenToShow = "#gameover-bad";
        let fail_reason;
        if(stats.school <= 0 && stats.family <= 0)
            fail_reason = "both your school and family meters simultaneously!";
        else if(stats.school <= 0)
            fail_reason = "your school meter";
        else
            fail_reason = "your family meter";
        $("#fail_reason").text(fail_reason);
    }
    else {
        // Good Ending
        screenToShow = "#gameover-good";
    }
    $(screenToShow).addClass("visible");
    $("#apps").addClass("app-visible");
    $("#viewport").attr("data-active", "none");
    gameState = GameState.Finished;
}

function showEndOfDayScreen() {
    $(`#end-of-day`).addClass("visible");
    $("#apps").addClass("app-visible");
    $("#viewport").attr("data-active", "none")
}

function allNightTimeDlt() {
    assignments.filter(x => x.progress < x.numHrs && x.dueDay >= currentDay).forEach((x, i) => {
        x.progress++;
        $(`.planner-task[data-id=${i}] .planner-task-status`).text(`${x.progress} / ${x.numHrs} hrs`);
        if(x.progress >= x.numHrs) {
            $(`.planner-task[data-id=${i}]`).addClass("completed");
        }
    });
    stats.energy = 4;
    updateStats();
}

function endDay() {
    updateStats({ school: -notifications.mail.count, family: -notifications.messages.count + (phoneCallPromised ? -4 : 0) });
    $("#messages-history").append($("<div/>", { class: "messages-spacer" }));
    days[++currentDay].events();
    $("#start-day-text").text(`Start ${daysOfWeek[currentDay]}`);
    doDailyMessage(currentDay);
    updateNotifications();
    assignments.forEach((x, i) => {
        if(x.dueDay < currentDay && x.progress < x.numHrs && !x.penalized) {
            x.penalized = true;
            $(`.planner-task[data-id="${i}"]`).addClass("overdue");
            updateStats({ school: x.progress - x.numHrs });
            notifications.planner.count--;
            const length = assignments.filter(x => x.progress < x.numHrs && x.dueDay >= currentDay).length;
            $("#app-icons .app-button[data-app='planner'] .app-notification-badge").attr("data-count", length).find(".badge-text").text(length);
        }
    });
    if(gameState == GameState.Finished)
        return;
    $(".app").removeClass("visible");
    $("#charging").addClass("visible");
    $("#alarm").addClass("visible");
    setTimeout(() => {
        $("#charging").removeClass("visible");
        showAlarmClock();
    }, 3000);
    $('#app-mail').attr('data-screen', 'inbox');
}

$(() => {
    onresize();

    $(".app-button").on("click", startApp);

    var url = new URL(window.location);
    if(url.hostname == "localhost" || url.protocol == "file:") {
        if(url.searchParams.has("show")) {
            gameState = GameState.InPlay;
            $(".visible").removeClass("visible");
            $(`#${url.searchParams.get("show")}`).addClass("visible");
            $("#viewport").attr("data-active", url.searchParams.get("show").replace(/^app-/, ""));
        }

        if(url.searchParams.has("day")) {
            gameState = GameState.InPlay;
            currentDay = url.searchParams.get("day");
            days.slice(0, currentDay + 1).forEach((x, i) => doDailyMessage(i) || x.events());
            updateNotifications();
        }

        if(url.searchParams.has("read")) {
            $("#mail-inbox-messages .mail-inbox-message").each((i, x) => $(x).click());
        }
    }

    updateStats();
});

function homeButtonPress() {
    if(gameState == GameState.OnLockScreen) {
        gameState = GameState.InPlay;
        homeButtonPress();
    }
    if(gameState == GameState.InPlay) {
        $("#apps").removeClass("app-visible");
        $(".app").removeClass("visible");
        $("#viewport").attr("data-active", "none").removeClass("start-of-game");
    }
    else if(gameState == GameState.NotStarted) {
        $("#start-game").removeClass("visible");
        $("#apps").addClass("app-visible");
        $("#viewport").removeClass("start-of-game").attr("data-active", "none");
        doDailyMessage(currentDay);
        days[currentDay].events();
        updateNotifications();
        showAlarmClock();
    }
}

function showAlarmClock() {
    currentTime = 7;
    afterSpendTime();
    $("#statusbar .fa-battery-empty").removeClass("fa-battery-empty").addClass("fa-battery-full");
    $("#alarm span").css("animation", "blink 1s 3");
    gameState = GameState.ShowingAlarmClock;
    $("#lockscreen").addClass("visible");
    $("#statusbar-middle").text(daysOfWeek[currentDay]);
    setTimeout(() => {
        gameState = GameState.InPlay;
        $("#alarm").removeClass("visible");
        $("#alarm span").css("animation", "none");
    }, 3000);
}

function startApp(ev) {
    $("#messages-history").scrollTop($("#messages-history").height());
    $(`#app-${$(ev.currentTarget).data("app")}`).addClass("visible");
    $("#apps").addClass("app-visible");
    $("#viewport").attr("data-active", $(ev.currentTarget).data("app"));
}
